<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Demo");
?>

<p>rusbelt.ru</p>

<?php
$APPLICATION->IncludeComponent(
    'demo:elements',
    '.default',
    [
        'IBLOCK_TYPE' => 'catalog',
        'IBLOCK_ID' => '2',
        'FILTER_PROP_CODE' => 'IS_NEW',
    ],
    false
);
?>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>