<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class DemoElementsComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {

        $result = [
            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
            'CACHE_TIME' => $arParams['CACHE_TIME'] ?? 36000000,
        ];

        return array_merge($result, $arParams);
    }

    public function checkModules()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
    }

    public function getElements()
    {
        $code = $this->arParams['FILTER_PROP_CODE'];

        // Создаем свойство
        if (!CIBlockProperty::GetList([], ['CODE' => $code, 'IBLOCK_ID' => $this->arParams['IBLOCK_ID']])->Fetch()) {
            $property = new CIBlockProperty();
            $propertyId = $property->Add([
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'NAME' => 'Новинка',
                'CODE' => $code,
                'PROPERTY_TYPE' => 'L',
                'LIST_TYPE' => 'C',
            ]);

            if ($propertyId) {
                $propertyEnum = new CIBlockPropertyEnum();

                $propertyEnum->Add([
                    'PROPERTY_ID' => $propertyId,
                    'VALUE' => 'Да',
                    'XML_ID' => 'Y',
                ]);
            }
        }

        // Выбираем элементы
        $elements = [];

        $arFilter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            '!PROPERTY_' . $code => false,
        ];

        $dbRes = CIBlockElement::GetList(['SORT' => 'ASC', 'ID' => 'DESC'], $arFilter);
        while ($obRes = $dbRes->GetNextElement()) {
            $arRes = $obRes->GetFields();
            $arRes['PROPERTIES'] = $obRes->GetProperties();

            $elements[] = $arRes;
        }

        return $elements;
    }

    public function prepareResult()
    {
        $this->arResult['ITEMS'] = $this->getElements();
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->checkModules();
            $this->prepareResult();

            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }

}
