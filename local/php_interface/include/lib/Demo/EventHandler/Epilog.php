<?php

namespace Demo\EventHandler;

class Epilog
{
    public static function OnEndBufferContent(&$content)
    {
        $content = str_replace('rusbelt.ru', 'test.rusbelt.ru', $content);
    }
}
